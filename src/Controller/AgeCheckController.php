<?php

  /**
   * @file
   * Contains \Drupal\first_module\Controller\FirstController.
   */

  namespace Drupal\age_check\Controller;

  use Drupal\Core\Controller\ControllerBase;

  class AgeCheckController extends ControllerBase {
    public function content() {
      return [
        '#type'   => 'markup',
        '#markup' => t('Age Check Fail'),
      ];
    }
  }
