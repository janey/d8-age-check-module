<?php
  /**
   * @file
   * Contains \Drupal\age_check\Form\AgeCheckForm.
   */

  namespace Drupal\age_check\Form;

  use Drupal\Core\Form\FormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Url;

  /**
   * Contribute form.
   */
  class AgeCheckForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'age_check_contribute_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $form['country'] = [
        '#type'          => 'select',
        '#title'         => t('Choose your country*'),
        '#options'       => [
          'ZA' => t('South Africa'),
          'NA' => t('Namibia'),
          'BW' => t('Botswana'),
          'LS' => t('Lesotho'),
        ],
        '#default_value' => 'ZA',
      ];
      $form['day'] = [
        '#title'         => t('Enter your date of birth*'),
        '#type'        => 'textfield',
        '#required'    => TRUE,
        '#placeholder' => 'DD',
        '#maxlength' => '2',
      ];
      $form['month'] = [
        '#type'        => 'textfield',
        '#required'    => TRUE,
        '#placeholder' => 'MM',
        '#maxlength' => '2',
      ];
      $form['year'] = [
        '#type'        => 'textfield',
        '#required'    => TRUE,
        '#placeholder' => 'YYYY',
        '#maxlength' => '4',
      ];

      $form['submit'] = [
        '#type'  => 'submit',
        '#value' => t('<span>#GO</span>BOLD'),
      ];
      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

      $day = $form_state->getValue('day');
      $month = $form_state->getValue('month');
      $year = $form_state->getValue('year');

      if(!is_numeric ($day) || !is_numeric ($month) || !is_numeric ($year) || $day > 31 || $month > 12){
        // if none of the date fields are numeric show an error
        $invalid_input_date = array(
          '%day' => $form_state->getValue('day'),
          '%month' => $form_state->getValue('month'),
          '%year' => $form_state->getValue('year'),
        );

        $form_state->setErrorByName('year', $this->t("The date <strong>%day / %month / %year</strong>  is invalid.", $invalid_input_date));
      }
      $age = $this->checkAge($form_state);

      if($age > 100){
        $form_state->setErrorByName('year', $this->t("Are you sure you are %age years old?", array('%age' => $age)));
      }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

      $age = $this->checkAge($form_state);

      if ($age < '18') {
        // if less than 18 display the age_check.fail route
        $url = Url::fromRoute('age_check.fail');
        $form_state->setRedirectUrl($url);
      } else {
        // set cookie so we know this user has passed the age check
        setcookie('age_check_ok', 1, time()+86400, '/');

        if(array_key_exists('age_checked_path', $_COOKIE)){
          $original_path = $_COOKIE['age_checked_path'];
        }

        if (!empty($original_path) && $original_path != '/') {
          $redirect_url = Url::fromUserInput($original_path);
          // Add a redirect to requested page. Using $form_state built in redirects.
          $form_state->setRedirectUrl($redirect_url);
        } else {
          // For everything else, redirect to homepage.
          $form_state->setRedirect('<front>');
        }
      }

    }

    public static function checkAge(FormStateInterface $form_state){
      $day = $form_state->getValue('day');
      $month = $form_state->getValue('month');
      $year = $form_state->getValue('year');

      $timezone = new \DateTimeZone('Africa/Johannesburg');
      $age =
        \DateTime::createFromFormat('d/m/Y', $day . '/' . $month . '/' . $year,
          $timezone)->diff(new \DateTime('now', $timezone))->y;

      return $age;
    }
  }

?>
