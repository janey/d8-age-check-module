<?php

  /**
   * @file
   * Contains \Drupal\age_check\EventSubscriber\AgeCheckSubscriber.
   */

  namespace Drupal\age_check\EventSubscriber;

  use Symfony\Component\HttpKernel\Event\GetResponseEvent;
  use Symfony\Component\HttpKernel\KernelEvents;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;
  use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
  use Symfony\Component\HttpFoundation\RedirectResponse;

  /**
   * Class AgeCheckSubscriber.
   */
  class AgeCheckSubscriber implements EventSubscriberInterface {

    /**
     * Flag to indicate if reports should be written to watchdog.
     *
     * @var bool $report
     */
    private $log_report;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
      $events[KernelEvents::REQUEST] = array('ageCheckRedirect', 27);
      return $events;
    }

    /**
     * Checks for age_check cookie, and redirects to age check form if not set.
     *
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $response
     *   The created response object that will be returned.
     * @param string $event
     *   The string representation of the event.
     * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
     *   Event dispatcher that lazily loads listeners and subscribers from the dependency injection
     *   container.
     */
    public function ageCheckRedirect(GetResponseEvent $response, $event, ContainerAwareEventDispatcher $event_dispatcher) {
      $time_start = microtime(true);
      // if the age check cookie is set, exit early.
      if(isset($_COOKIE['age_check_ok'])){
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $this->report('Age Check: User has cookie set', $time, TRUE);
        return;
      }
      // get the request
      $request = $response->getRequest();
      $alias_manager = \Drupal::service('path.alias_manager');
      $request_alias = $alias_manager->getAliasByPath($request->getRequestUri());

      // get current user
      $user = \Drupal::currentUser()->getRoles();

      // array of user_agents to skip
      $user_agents = [
        'Googlebot',
        'Googlebot-Mobile',
        'Googlebot-Image',
        'bingbot',
        'msnbot',
        'slurp',
        'Facebot',
        'facebookexternalhit',
        'Google Page Speed',
      ];

      // incoming user agent
      $http_user_agent = \Drupal::request()->server->get('HTTP_USER_AGENT');

      foreach ($user_agents as $user_agent) {
        if (strlen(strstr($http_user_agent, $user_agent)) > 0) {
          return;
        }
      }

      $skipped_aliases = [
        '/age-check',
        '/age-check/fail',
        '/user/login'
      ];

      if(!isset($_COOKIE['age_check_ok']) && !in_array($request_alias, $skipped_aliases) && !in_array("administrator", $user)){
        //if coming from the age check form, don't set the age_check_path
        if($request_alias != '/age-check'){
          setcookie('age_checked_path', $request_alias, time()+3600, '/');
        }
        $returnResponse = new RedirectResponse('/age-check', 301);
        $response->setResponse($returnResponse);

        $time_end = microtime(true);
        $time = $time_end - $time_start;
        $this->report('Age Check: User should be directed to age check', $time, TRUE);
      }

      $time_end = microtime(true);
      $time = $time_end - $time_start;
      if(in_array($request_alias, $skipped_aliases)){
        $this->report('Age Check: User is viewing '.$request_alias, $time, TRUE);
      }

    }

    /**
     * {@inheritdoc}
     */
    public function report($name, $time, $report = FALSE) {
      if ($report || $this->log_report) {
        \Drupal::logger('age_check')
          ->notice('@function : @time (msec)', [
            '@function' => $name,
            '@time'     => number_format($time * 1000, 2),
          ]);
      }
    }

  }
